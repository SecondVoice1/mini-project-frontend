FROM node:18-alpine AS builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM caddy:2-alpine
WORKDIR /app
COPY --from=builder /app/dist .

